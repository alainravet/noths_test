How to run the tests:

    $ bundle exec rake

```
$ bundle exec rake
Started with run options --seed 5810


  ============================================================
  Acceptance tests: applying promotion rules to baskets of items
  ============================================================
  test_0001_example 1                                             PASS (0.00s)
  test_0002_example 2                                             PASS (0.00s)
  test_0003_example 3                                             PASS (0.00s)

#<PromotionRule:0x007f9a5b07aa88>::#discount_for(items)::when only 1 travel card holder is ordered
  test_0001_is 0                                                  PASS (0.00s)

PriceCalculator::#total::when a single 10%-discount promotion is applied
  test_0001_updates the #discount and #total values accordingly   PASS (0.00s)

#<PromotionRule:0x007f9a5b0598b0>::#discount_for(items)::when the items cost 61*100 cents
  test_0001_is 10%                                                PASS (0.00s)

#<PromotionRule:0x007f9a5b07aa88>::#discount_for(items)::when 2 travel card holders are ordered
  test_0001_returns a discount as if the travel card holders cost 8.50 PASS (0.00s)

#<PromotionRule:0x007f9a5b0598b0>::#discount_for(items)::when the items cost 60*100 cents
  test_0001_is 10%                                                PASS (0.00s)

PriceCalculator::#total::when two (non-commutative) promotion rules are applied in sequence
  test_0001_updates the #discount and #total values accordingly   PASS (0.00s)

Finished in 0.00369s
9 tests, 13 assertions, 0 failures, 0 errors, 0 skips
alain@Alains-MacBook-Pro:~/dev/_recruit/noths_test (master)
```

Problem description:

    notonthehighstreet.com is an online marketplace, here is a sample of some of the products available on our site:

      Product code | Name                   | Price
    ----------------------------------------------------------
        001        | Travel Card Holder     | £9.25
        002        | Personalised cufflinks | £45.00
        003        | Kids T-shirt           | £19.95


    Our marketing team want to offer promotions as an incentive for our customers to purchase these items.

    If you spend over £60, then you get 10% off your purchase
    If you buy 2 or more travel card holders then the price drops to £8.50.

    Our check-out can scan items in any order, and because our promotions
    will change, it needs to be flexible regarding our promotional rules.

    The interface to our checkout looks like this (shown in Ruby):
      co = Checkout.new(promotional_rules)
      co.scan(item)
      co.scan(item)
      price = co.total

    Implement a checkout system that fulfills these requirements.

    Test data
    ---------
    Basket: 001, 002, 003 -> Total price expected: £66.78
      9.25  + 45.00 + 19.95 = 74.2
      74.2 * 0.9 = 66.78                        # total >= 60 -> 10% discount

    Basket: 001, 003, 001 -> Total price expected: £36.95
      9.25 + 19.95 + 9.25
      8.50 + 19.95 + 8.50                       # 2 of 001 -> 8.50 each
      8.50 + 19.95 + 8.50 = 36.95               # less than 60 -> no discount

    Basket: 001, 002, 001, 003 -> Total price expected: £73.76
        9.25  + 45.00 + 9.25  + 19.95   # discount: 1.50
        8.50  + 45.00 + 8.50  + 19.95           # 2 of 001 -> 8.50 each
        8.50  + 45.00 + 8.50  + 19.95 = 81.95  # discount = 819.5
        81.95 * 0.9 = 73.76                     # total >= 60 -> 10% discount

