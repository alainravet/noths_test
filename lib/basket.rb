class Basket
  attr_reader :items, :discount

  def initialize(items = nil, discount = 0)
    @items = Array(items)
    @discount = discount
  end

  alias discount_in_cents discount

  def add_item(item)
    @items << item if item
  end

  def add_discount(new_discount)
    @discount += new_discount
  end

  def total_in_cents
    items.map(&:price_in_cents).sum - discount
  end
end
