class Checkout
  def initialize(promotion_rules)
    @promotion_rules = Array(promotion_rules).compact

    @basket = Basket.new
  end

  def scan(item)
    @basket.add_item(item)
  end

  def total_in_cents
    PriceCalculator.new(@basket)
                   .apply(@promotion_rules)
                   .total_in_cents
  end
  alias total total_in_cents
end
