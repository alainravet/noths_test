require 'item'

module NOTHS
  module Catalogue
    ITEM_001 = Item.new('001', 'Travel Card Holder',      9.25 * 100)
    ITEM_002 = Item.new('002', 'Personalised cufflinks', 45.00 * 100)
    ITEM_003 = Item.new('003', 'Kids T-shirt',           19.95 * 100)

    TRAVEL_CARD_HOLDER = ITEM_001
  end
end
