module NOTHS
  module PromotionRules
  end
end

# Load the rules
require 'promotion_rule'
Dir[File.join(File.dirname(__FILE__), 'promotion_rules/**/*.rb')].sort.each { |f| require f }
