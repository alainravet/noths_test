module NOTHS
  module PromotionRules
    MULTIPLE_TRAVEL_CARD_HOLDERS = PromotionRule.new.tap do |rule|
      def rule.deserves_discount?
        2 <= travel_card_holders_count(items)
      end

      def rule.additional_discount
        discount_per_tvc * travel_card_holders_count(items)
      end

      def rule.travel_card_holders_count(items)
        items.select { |item| item == NOTHS::Catalogue::TRAVEL_CARD_HOLDER }.count
      end

      def rule.discount_per_tvc
        full_price = NOTHS::Catalogue::TRAVEL_CARD_HOLDER.price_in_cents
        reduced_price = 8_50
        full_price - reduced_price
      end
    end
  end
end
