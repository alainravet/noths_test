module NOTHS
  module PromotionRules
    TEN_PERCENT_OFF_OVER_60 = PromotionRule.new.tap do |rule|
      REDUCTION_IN_PERCENT = 10
      def rule.deserves_discount?
        60_00 < basket.total_in_cents
      end

      def rule.additional_discount
        basket.total_in_cents / REDUCTION_IN_PERCENT
      end
    end
  end
end
