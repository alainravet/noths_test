class PriceCalculator
  def initialize(basket)
    @basket = basket
  end

  def total_in_cents
    @basket.total_in_cents
  end
  alias total total_in_cents

  def apply(promotion_rule_or_rules)
    Array(promotion_rule_or_rules).each do |rule|
      apply_one_rule(rule)
    end
    self
  end

private

  def apply_one_rule(promotion_rule)
    new_discount = promotion_rule.discount_for(@basket)
    @basket.add_discount(new_discount)
  end

  def total_in_cents_without_discount
    @basket.total_in_cents
  end
end
