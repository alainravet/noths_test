class PromotionRule
  attr_reader :basket

  def discount_for(basket)
    @basket = basket
    return 0 unless deserves_discount?
    additional_discount
  end

  def items
    @basket.items
  end

  def discount
    @basket.discount
  end

  def deserves_discount?
    raise "#deserves_discount? is not implemented in #{self.class}"
  end

  def additional_discount
    raise "#additional_discount is not implemented in #{self.class}"
  end
end
