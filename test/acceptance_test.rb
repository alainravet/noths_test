require_relative 'test_helper'

describe '
  ============================================================
  Acceptance tests: applying promotion rules to baskets of items
  ============================================================
' do
  let(:rules) do
    [
      NOTHS::PromotionRules::MULTIPLE_TRAVEL_CARD_HOLDERS,
      NOTHS::PromotionRules::TEN_PERCENT_OFF_OVER_60,
    ]
  end

  let(:item_001) { NOTHS::Catalogue::ITEM_001 }
  let(:item_002) { NOTHS::Catalogue::ITEM_002 }
  let(:item_003) { NOTHS::Catalogue::ITEM_003 }

  it 'example 1' do
    items          = [item_001, item_002, item_003]
    expected_price = 66_78
    basket_price_with_discount(items, rules).must_equal expected_price
  end

  it 'example 2' do
    items          = [item_001, item_003, item_001]
    expected_price = 36_95
    basket_price_with_discount(items, rules).must_equal expected_price
  end

  it 'example 3' do
    items          = [item_001, item_002, item_001, item_003]
    expected_price = 73_76
    basket_price_with_discount(items, rules).must_equal expected_price
  end

  def basket_price_with_discount(items, promotion_rules)
    Checkout.new(promotion_rules).tap do |co|
      items.each { |item| co.scan(item) }
    end.total.round
  end
end
