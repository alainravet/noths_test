require 'minitest/autorun'

# for Rubymine integration:
require 'minitest/reporters'
MiniTest::Reporters.use! [Minitest::Reporters::SpecReporter.new]

module Kernel
  alias context describe
end

require 'basket'
require 'checkout'
require 'item'
require 'promotion_rule'
require 'price_calculator'
require 'noths/catalogue'
require 'noths/promotion_rules'
