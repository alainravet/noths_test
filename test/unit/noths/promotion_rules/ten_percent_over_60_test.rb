require_relative '../../../test_helper'

describe NOTHS::PromotionRules::TEN_PERCENT_OFF_OVER_60, '#discount_for(items)' do
  context 'when the items cost 60*100 cents' do
    it 'is 10%' do
      items = [
        Item.new(_, _, 60 * 100),
      ]
      basket = Basket.new(items)
      NOTHS::PromotionRules::TEN_PERCENT_OFF_OVER_60.discount_for(basket).must_equal 0
    end
  end

  context 'when the items cost 61*100 cents' do
    it 'is 10%' do
      items = [
        Item.new(_, _, 61 * 100),
      ]
      basket = Basket.new(items)
      NOTHS::PromotionRules::TEN_PERCENT_OFF_OVER_60.discount_for(basket).must_equal 61 * 10
    end
  end
end
