require_relative '../../../test_helper'

describe NOTHS::PromotionRules::MULTIPLE_TRAVEL_CARD_HOLDERS, '#discount_for(items)' do
  context 'when only 1 travel card holder is ordered' do
    it 'is 0' do
      items = [
        Item.new(_, _, 666 * 100),
        NOTHS::Catalogue::TRAVEL_CARD_HOLDER,
      ]
      basket = Basket.new(items)
      NOTHS::PromotionRules::MULTIPLE_TRAVEL_CARD_HOLDERS.discount_for(basket).must_equal 0
    end
  end

  context 'when 2 travel card holders are ordered' do
    it 'returns a discount as if the travel card holders cost 8.50' do
      items = [
        NOTHS::Catalogue::TRAVEL_CARD_HOLDER,
        Item.new(_, _, 666 * 100),
        NOTHS::Catalogue::TRAVEL_CARD_HOLDER,
      ]
      basket = Basket.new(items)
      price_before_discount = NOTHS::Catalogue::TRAVEL_CARD_HOLDER.price_in_cents
      expected_discount = 2 * (price_before_discount - 8_50)
      NOTHS::PromotionRules::MULTIPLE_TRAVEL_CARD_HOLDERS.discount_for(basket).must_equal expected_discount
    end
  end
end
