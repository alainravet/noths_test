require_relative '../test_helper'

describe PriceCalculator, '#total' do
  TEN_PCT_OFF = PromotionRule.new.tap do |rule|
    def rule.deserves_discount?
      true
    end

    def rule.additional_discount
      basket.total_in_cents / 10
    end
  end

  HUNDRED_CENTS_OFF = PromotionRule.new.tap do |rule|
    def rule.deserves_discount?
      true
    end

    def rule.additional_discount
      100
    end
  end

  context 'when a single 10%-discount promotion is applied' do
    it 'updates the #discount and #total values accordingly' do
      items = [
        Item.new(_, _, 100),
        Item.new(_, _, 660),
      ]
      basket = Basket.new(items)
      PriceCalculator.new(basket).tap do |calc|
        calc.apply(TEN_PCT_OFF)

        basket.discount_in_cents.must_equal (100 + 660) * 0.1
        basket.total_in_cents   .must_equal (100 + 660) * 0.9
      end
    end
  end

  context 'when two (non-commutative) promotion rules are applied in sequence' do
    it 'updates the #discount and #total values accordingly' do
      items = [
        Item.new(_, _, 1000),
        Item.new(_, _, 2000),
      ]
      basket = Basket.new(items)
      PriceCalculator.new(basket).tap do |calc|
        calc.apply(HUNDRED_CENTS_OFF)
        basket.discount_in_cents.must_equal 100
        basket.total_in_cents   .must_equal ((1000 + 2000) - 100)

        calc.apply(TEN_PCT_OFF)
        basket.discount_in_cents.must_equal 100 + ((1000 + 2000) - 100) * 0.1
        basket.total_in_cents   .must_equal ((1000 + 2000) - 100) * 0.9
      end
    end
  end
end
